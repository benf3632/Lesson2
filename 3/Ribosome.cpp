#include "Ribosome.h"

using std::string;

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* protein = new Protein;
	AminoAcid amino_acid;
	string substr;

	protein->init();
	while (RNA_transcript.size() >= 3)
	{
		substr = RNA_transcript.substr(0, 3); //gets a codon
		if ((amino_acid = get_amino_acid(substr)) != UNKNOWN) //checks if the codon has an amino Acid
		{
			protein->add(amino_acid); //adds the amino acid to protein
			RNA_transcript.erase(0, 3); //erases a codon from the transcript
		}
		else
		{
			protein->clear(); //clears protein if codon is not matching a single amino acid
			delete protein;
			return nullptr;
		}
	}

	return protein;
}