#pragma once
#include "Protein.h"

class Mitochondrion {
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;

public:
	//init
	void init();

	//setters
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose_level(const unsigned int glocuse_units);

	bool produceATP(const int glocuse_unit) const;
};