#include "Nucleus.h"

using std::string;

//init gene
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//getters for gene
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

//setters for gene
void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}

void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//inits nucleus
void Nucleus::init(const string dna_sequence)
{
	if (Nucleus::is_dna_strand_valid(dna_sequence))
	{
		this->_DNA_strand = dna_sequence;
		this->_complementary_DNA_strand = "";

		for (int i = 0; i < dna_sequence.size(); i++)
		{
			if (dna_sequence[i] == 'C') {
				this->_complementary_DNA_strand += 'G';
			}
			else if (dna_sequence[i] == 'G') {
				this->_complementary_DNA_strand += 'C';
			}
			else if (dna_sequence[i] == 'T') {
				this->_complementary_DNA_strand += 'A';
			}
			else if (dna_sequence[i] == 'A') {
				this->_complementary_DNA_strand += 'T';
			}
		}
	}
	else
	{
		std::cerr << "DNA strand is Invalid!" << std::endl;
		getchar();
		_exit(1);
	}
}

//checks if the dna strand is valid
bool Nucleus::is_dna_strand_valid(const string dna_sequence)
{
	for (int i = 0; i < dna_sequence.size(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'T' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C') {
			return false;
		}
	}
	return true;
}

//gets rna transcript from dna strand
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string rna_transcript = "";
	if (gene.is_on_complementary_dna_strand())
	{
		//complementary dna_strand RNA
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_complementary_DNA_strand[i] == 'T')
			{
				rna_transcript += 'U';
			}
			else
			{
				rna_transcript += this->_complementary_DNA_strand[i];
			}
		}
	}
	else
	{
		//dna_strand RNA
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_DNA_strand[i] == 'T')
			{
				rna_transcript += 'U';
			}
			else
			{
				rna_transcript += this->_DNA_strand[i];
			}
		}
	}
	return rna_transcript;
}

//reverses the dna_strand
string Nucleus::get_reversed_DNA_strand() const
{
	string reversed_DNA_strand = "";
	for (int i = this->_DNA_strand.size() - 1; i >= 0; i--)
	{
		reversed_DNA_strand += this->_DNA_strand[i];
	}
	return reversed_DNA_strand;
}

//gets the number of a certain codon in the dna_strand
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int pos = 0, count = 0;
	while ((pos = this->_DNA_strand.find(codon, pos)) != string::npos)
	{
		pos++;
		count++;
	}
	return count;
}