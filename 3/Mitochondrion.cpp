#include "Mitochondrion.h"

//init Mitochondrion
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

//inserts glucose_receptor according to the protein
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcid amino_acid_scheme[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	int order = 0;
	AminoAcidNode* temp = protein.get_first();
	bool flag = true;
	while (temp != nullptr && flag == true)
	{
		if (temp->get_data() == amino_acid_scheme[order])
		{
			temp = temp->get_next();
			order++;
		}
		else
		{
			this->_has_glocuse_receptor = false;
			flag = false;
		}
	}

	if (order == 7)
	{
		this->_has_glocuse_receptor = true;
	}
}

//sets glocuse level
void Mitochondrion::set_glucose_level(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50)
	{
		return true;
	}
	else
	{
		return false;
	}
}