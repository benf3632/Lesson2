#include "Cell.h"

using std::string;

//init cell
void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
}

bool Cell::get_ATP()
{
	string rna_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* protein = this->_ribosome.create_protein(rna_transcript);
	if (protein == nullptr)
	{
		std::cerr << "Failed to create protein" << std::endl;
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*protein);
	this->_mitochondrion.set_glucose_level(50);
	if (this->_mitochondrion.produceATP(50))
	{
		this->_apt_units = 100;
		return true;
	}
	else
	{
		return false;
	}
}